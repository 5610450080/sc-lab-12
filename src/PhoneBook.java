import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;



public class PhoneBook {
	
	private String filename = "phonebook.txt";
	private FileReader fileReader = null;
	private FileWriter fileWriter = null;
	private String s,j;
	
	public String fileReader() throws IOException{
		try {
			fileReader = new FileReader(filename);
			BufferedReader bf = new BufferedReader(fileReader);
			String line;
			for (line = bf.readLine(); line != null;line = bf.readLine()){
				s = line;
			}
		}
		catch (FileNotFoundException e){
			s = "Cannot read file " + filename;
		}
		catch (IOException e){
			s = "Error reading from file";
		}
		finally{
			j = s ;
			try {
				if (fileReader != null)
					fileReader.close();
			} 
			catch (IOException e) {
				j = "Error closing files";
			}
		}
		
		return j;
	}
	
	public String fileWriter() throws IOException{
		try {
			InputStreamReader inReader = new InputStreamReader(System.in);
			BufferedReader bf = new BufferedReader(inReader);

			fileWriter = new FileWriter("phonebook.txt",true);
			BufferedWriter out = new BufferedWriter(fileWriter);
	
			String line = bf.readLine();
			while (!line.equals("end")) { 
				out.write(line);
				out.newLine();
				line = bf.readLine();
			}
			out.flush();
		}
		catch (IOException e){
			s = "Error reading from user";
		}
		finally {
			j = s;
			try {
				if (fileWriter != null)
					fileWriter.close();
			} 
		catch (IOException e) {
			j = "Error closing files";
		}
	}
		return j;
}
	
	}
	
	

	
	

	
