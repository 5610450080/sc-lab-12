import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;


public class HomeWork {
	public void main(String[] args){
		String filename = "homework.txt";
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		ArrayList<Double> ar = new ArrayList<>();
		ArrayList<String> array = new ArrayList<>();
		try {
			String name = " ";
			fileReader = new FileReader(filename);
			BufferedReader bf = new BufferedReader(fileReader);
			String line;
			for (line = bf.readLine(); line != null; line = bf.readLine()) {
				String str[] = line.split("");
				for (int i=1;i<str.length;i++){
					double d = Double.parseDouble(str[i]);
					ar.add(d);
				}
				double av = average(ar);
				array.add(str[0]+" "+av);
			}
			fileWriter = new FileWriter("average.txt");
			PrintWriter pw = new PrintWriter(fileWriter);
			for (int i = 0;i<array.size();i++){
				pw.println(name + array.get(i));
				pw.flush();
			}
		}
		catch (IOException e){
			System.err.println("Error reading from user");
		}
		finally {
			System.out.println("FINALLY");
			try {
				if (fileWriter != null)
					fileWriter.close();
			} 
			catch (IOException e) {
				System.err.println("Error closing files");
			} 
		}
	}
	public static double average(ArrayList<Double> b){
		double sum = 0;
		for(int i = 0 ; i<b.size();i++){
			sum = sum+b.get(i);
			
		}
		return sum/b.size();
		
	}
}

	


