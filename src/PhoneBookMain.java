import java.io.IOException;
import java.util.concurrent.FutureTask;


public class PhoneBookMain {
	public static void main(String[] args){
		PhoneBook PB = new PhoneBook();
		
			System.out.println("-------JAVA PHONE BOOK-------");
			System.out.println("Name   Phone");
			System.out.println("====  =======");
			
			try {
				System.out.println(PB.fileReader());
				System.out.println("Input phone to PhoneBook : ");
			} catch (IOException e) {
				System.out.println("Error : "+ e.getMessage());
			}
			
			try {
				System.out.println(PB.fileWriter());
				System.out.println(PB.fileReader());
			}
			catch (IOException e){
				System.out.println("Error : "+ e.getMessage());
			}
	
			
		
		
	}

}
